from mastodon import Mastodon

access_token = 'access.token'
# in the file access.token you have pasted the long line of characters.
# the access.token file should not be part of your GIT files.

api = Mastodon(
    access_token = access_token,
    api_base_url = 'https://botsin.space/'
)

# Toot your first toot
api.toot("howdy universe!")

# Toot your first image toot
img = 'test.jpeg'
media = api.media_post(img, "image/jpeg")
api.status_post("What a great image!", media_ids=media)