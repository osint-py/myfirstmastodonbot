# myFirstMastodonBot

1. Follow instructions to create account from [Shkspr][bot-link]
1. Follow instructions to create secret file with access_token

[bot-link]: https://shkspr.mobi/blog/2018/08/easy-guide-to-building-mastodon-bots

# References
1. https://shkspr.mobi/blog/2018/08/easy-guide-to-building-mastodon-bots/
1. https://BotsIn.Space/ 
1. https://botsin.space/settings/applications
1. https://github.com/halcy/Mastodon.py
1. https://mastodonpy.readthedocs.io/en/stable/
1. https://mastodonpy.readthedocs.io/en/stable/index.html?highlight=log_in#mastodon.Mastodon.log_in
1. https://linuxfr.org/users/omc/journaux/le-bloboscope
1. https://docs.joinmastodon.org/client/token/
1. https://gist.github.com/aparrish/661fca5ce7b4882a8c6823db12d42d26
1. https://github.com/aparrish/iceboxbreakfast
1. https://www.linux-magazine.com/Issues/2020/231/Social-Skills
1. https://docs.joinmastodon.org/api/oauth-scopes/
1. https://mcole18.github.io/documentation/docs/entities/error/
1. https://github.com/mastodon/mastodon/issues/1367


```python
from mastodon import Mastodon
api = Mastodon(client_id, client_secret, access_token, api_base_url="https://botsin.space")
api.toot("howdy universe!")
```

```python
  # Then login. This can be done every time, or use persisted.

  from mastodon import Mastodon

  mastodon = Mastodon(
      client_id = 'pytooter_clientcred.secret',
      api_base_url = 'https://mastodon.social'
  )
  mastodon.log_in(
      'my_login_email@example.com',
      'incrediblygoodpassword',
      to_file = 'pytooter_usercred.secret'
  )
```

```python
  from mastodon import Mastodon

  #   Set up Mastodon
  mastodon = Mastodon(
      access_token = 'token.secret',
      api_base_url = 'https://botsin.space/'
  )

  mastodon.status_post("hello world!")
```

```python
  # To post, create an actual API instance.

  from mastodon import Mastodon

  mastodon = Mastodon(
      access_token = 'pytooter_usercred.secret',
      api_base_url = 'https://mastodon.social'
  )
  mastodon.toot('Tooting from python using #mastodonpy !')
```

```python
from mastodon import Mastodon

mastodon = Mastodon(client_id = 'pytooter_clientcred.secret', api_base_url = 'https://votre_instance_mastodon_préféré')
mastodon.log_in('votre_login', 'votre_mdp', to_file = 'pytooter_usercred.secret')

```


```txt
Mastodon.log_in(username=None, password=None, code=None, redirect_uri='urn:ietf:wg:oauth:2.0:oob', refresh_token=None, scopes=['read', 'write', 'follow', 'push'], to_file=None)[source]
Get the access token for a user.

The username is the e-mail used to log in into mastodon.

Can persist access token to file to_file, to be used in the constructor.

Handles password and OAuth-based authorization.

Will throw a MastodonIllegalArgumentError if the OAuth or the username / password credentials given are incorrect, and MastodonAPIError if all of the requested scopes were not granted.

For OAuth2, obtain a code via having your user go to the url returned by auth_request_url() and pass it as the code parameter. In this case, make sure to also pass the same redirect_uri parameter as you used when generating the auth request URL.

Returns the access token as a string.
```
